package ru.t1.zkovalenko.tm.command.system;

import ru.t1.zkovalenko.tm.util.FormatUtil;

public class SystemInfoCommand extends AbstractSystemCommand {

    public static final String NAME = "info";

    public static final String ARGUMENT = "-i";

    public static final String DESCRIPTION = "Info about system";

    @Override
    public void execute() {
        System.out.println("Available processors (cores): " + Runtime.getRuntime().availableProcessors());

        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.convertBytes(freeMemory));

        final long maxMemory = Runtime.getRuntime().maxMemory();
        String maxMemoryChecked = maxMemory == Long.MAX_VALUE ? "no limit" : FormatUtil.convertBytes(maxMemory);
        System.out.println("Maximum memory: " + maxMemoryChecked);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + FormatUtil.convertBytes(totalMemory));

        System.out.println("Usage memory: " + FormatUtil.convertBytes(totalMemory - freeMemory));
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
