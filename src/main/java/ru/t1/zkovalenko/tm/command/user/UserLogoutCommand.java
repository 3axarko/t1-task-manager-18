package ru.t1.zkovalenko.tm.command.user;

import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class UserLogoutCommand extends AbstractUserCommand {

    private final String NAME = "logout";

    private final String DESCRIPTION = "User logout";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
