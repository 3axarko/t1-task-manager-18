package ru.t1.zkovalenko.tm.command.task;

import ru.t1.zkovalenko.tm.api.service.ITaskService;
import ru.t1.zkovalenko.tm.api.service.IProjectTaskService;
import ru.t1.zkovalenko.tm.command.AbstractCommand;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.exception.entity.TaskNotFoundException;
import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    protected void showTask(final Task Task) {
        if (Task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + Task.getId());
        System.out.println("NAME: " + Task.getName());
        System.out.println("DESCRIPTION: " + Task.getDescription());
        System.out.println("STATUS: " + Status.toDisplayName(Task.getStatus()));
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task + "[" + task.getId() + "]" + "\tBinded to: " + task.getProjectId());
            index++;
        }
    }

    @Override
    public String getArgument() {
        return null;
    }

}
