package ru.t1.zkovalenko.tm.command.project;

public class ProjectClearCommand extends AbstractProjectCommand {

    public static final String NAME = "project-clear";

    public static final String DESCRIPTION = "Remove all projects";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
