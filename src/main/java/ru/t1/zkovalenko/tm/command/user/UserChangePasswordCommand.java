package ru.t1.zkovalenko.tm.command.user;

import ru.t1.zkovalenko.tm.exception.field.PasswordEmptyException;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    private final String NAME = "change-password";

    private final String DESCRIPTION = "User change password";

    @Override
    public void execute() {
        final String userLogin = serviceLocator.getAuthService().getUser().getLogin();
        System.out.println("[CHANGE PASSWORD]");
        final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        serviceLocator.getUserService().setPassword(userLogin, password);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
