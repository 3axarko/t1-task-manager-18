package ru.t1.zkovalenko.tm.command.system;

import ru.t1.zkovalenko.tm.api.model.ICommand;
import ru.t1.zkovalenko.tm.command.AbstractCommand;

import java.util.Collection;

public class ApplicationExitCommand extends AbstractSystemCommand {

    public static final String NAME = "exit";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Quit from program";

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
