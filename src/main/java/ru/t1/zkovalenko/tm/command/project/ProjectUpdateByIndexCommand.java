package ru.t1.zkovalenko.tm.command.project;

import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-update-by-index";

    public static final String DESCRIPTION = "Update project by index";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        getProjectService().updateByIndex(index, name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
