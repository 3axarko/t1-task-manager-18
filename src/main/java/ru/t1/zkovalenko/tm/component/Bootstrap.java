package ru.t1.zkovalenko.tm.component;

import ru.t1.zkovalenko.tm.api.repository.ICommandRepository;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.api.repository.IUserRepository;
import ru.t1.zkovalenko.tm.api.service.*;
import ru.t1.zkovalenko.tm.command.AbstractCommand;
import ru.t1.zkovalenko.tm.command.project.*;
import ru.t1.zkovalenko.tm.command.system.*;
import ru.t1.zkovalenko.tm.command.task.*;
import ru.t1.zkovalenko.tm.command.user.*;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.exception.system.CommandNotSupportedException;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.repository.CommandRepository;
import ru.t1.zkovalenko.tm.repository.ProjectRepository;
import ru.t1.zkovalenko.tm.repository.TaskRepository;
import ru.t1.zkovalenko.tm.repository.UserRepository;
import ru.t1.zkovalenko.tm.service.*;
import ru.t1.zkovalenko.tm.util.ColorizeConsoleTextUtil;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new SystemInfoCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new ApplicationExitCommand());

        registry(new ProjectListCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());

        registry(new TaskListCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskShowByProjectIdCommand());

        registry(new UserRegistryCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserViewProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    private void processArgument(final String argument) {
        if (argument == null) throw new CommandNotSupportedException();
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new CommandNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void processCommand(final String command) {
        if (command == null) throw new CommandNotSupportedException();
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) processArgument(command);
        else abstractCommand.execute();
    }

    private void exit() {
        System.exit(0);
    }

    public void run(String[] args) {
        if (processArguments(args)) exit();

        initDemoData();
        initLogger();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter Command:");
                final String command = TerminalUtil.nextLine();
                System.out.println("---");
                processCommand(command);
                ColorizeConsoleTextUtil.greenText("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                ColorizeConsoleTextUtil.redText("[FAIL]");
                loggerService.error(e);
            }
        }
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void initLogger() {
        loggerService.info("** Welcome to Task-Manager **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** Task-Manager is shutting down **");
            }
        });
    }

    private void initDemoData() {
        projectService.add(new Project("project1", Status.IN_PROGRESS));
        projectService.add(new Project("project3", Status.NOT_STARTED));
        projectService.add(new Project("project2", Status.IN_PROGRESS));
        projectService.add(new Project("project4", Status.COMPLETED));

        taskService.create("task1");
        taskService.create("task2");

        userService.create("def", "test", "12@mail.ru");
        userService.create("def2", "test", "13@mail.ru");
        userService.create("admin", "test", Role.ADMIN);
    }

}
