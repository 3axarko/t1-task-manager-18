package ru.t1.zkovalenko.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

    void removeProjectByIndex(Integer index);

    void unbindTaskFromProject(String projectId, String taskId);

}
