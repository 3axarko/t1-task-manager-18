package ru.t1.zkovalenko.tm.exception.field;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class LoginEmptyException extends AbstractException {

    public LoginEmptyException() {
        super("Login is empty");
    }

}
