package ru.t1.zkovalenko.tm.exception.field;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class PasswordEmptyException extends AbstractException {

    public PasswordEmptyException() {
        super("Password is empty");
    }

}
