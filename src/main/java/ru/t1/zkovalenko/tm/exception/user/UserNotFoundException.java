package ru.t1.zkovalenko.tm.exception.user;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("User not Found");
    }

}
