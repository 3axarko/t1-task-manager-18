package ru.t1.zkovalenko.tm.exception.field;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class NameEmptyException extends AbstractException {

    public NameEmptyException() {
        super("Name is empty");
    }

}
