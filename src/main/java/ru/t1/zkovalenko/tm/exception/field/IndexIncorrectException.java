package ru.t1.zkovalenko.tm.exception.field;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Index is incorrect");
    }

}
