package ru.t1.zkovalenko.tm.exception.field;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class DescriptionEmptyException extends AbstractException {

    public DescriptionEmptyException() {
        super("Description is empty");
    }

}
