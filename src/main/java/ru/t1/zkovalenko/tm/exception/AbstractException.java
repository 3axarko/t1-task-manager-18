package ru.t1.zkovalenko.tm.exception;

public abstract class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(String message) {
        super("Error! " + message);
    }

    public AbstractException(String message, Throwable cause) {
        super("Error! " + message, cause);
    }

    public AbstractException(Throwable cause) {
        super(cause);
    }

    public AbstractException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super("Error! " + message, cause, enableSuppression, writableStackTrace);
    }

}